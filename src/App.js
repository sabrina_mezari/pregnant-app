import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import { GlobalProvider } from "./context/GlobalContext";
import { ThemeProvider } from "@material-tailwind/react";



import Home from "./pages/Home";
import DevBaby from "./pages/DevBaby";
import Pedagogy from "./pages/Pedagogy";

import "./styles/global.css";


function App() {
  return (
    <div className="App">
      <ThemeProvider>
        <GlobalProvider>
          <BrowserRouter>
            <Routes>
              <Route exact path="/" element={<Home />}/>
              <Route path="/dev-baby/" element={<DevBaby />}/>
              {/* <Route path="/dev-baby/:weekNumber" element={<DevBaby />}/> */}
              <Route path="/pedagogy" element={<Pedagogy />}/>
            </Routes>
          </BrowserRouter>
        </GlobalProvider>
      </ThemeProvider>
       
    </div>
  );
}

export default App;
