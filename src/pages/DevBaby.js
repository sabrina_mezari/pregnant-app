import { useContext, useEffect, useState } from "react";
import { GlobalContext } from "../context/GlobalContext";
import NavigationBar from "../components/NavBar";
import "../styles/index.css";

import { 
    Typography,
  } from "@material-tailwind/react";


import datas from "../components/Datas";
import SymptomsCard from "../components/SymptomsCard";



function DevBaby() {
    
    const { globalContext } = useContext(GlobalContext);
    
    const [currentWeekData, setCurrentWeekData] = useState(null);
    const [currentTrimesterData, setCurrentTrimesterData] = useState(null);

       
    useEffect(() => {
        //Calcul de la différence entre la dateOfPregnancy et la date du jour. Ce calcul est en semaine.
        const diffTime = new Date().getTime() - new Date(globalContext.dateOfPregnancy).getTime();
        const diffWeek = Math.round(diffTime / (1000 * 3600 * 24 * 7));

        //Calcul de la différence entre la dateOfPregnancy et la date du jour. Ce calcul est en trimestre.
        const diffTrimester = Math.floor((diffWeek / 12 ) + 1);

        //faire un find pour trouver l'entrée correspondant à la semaine dans data, puis faire un state avec cette data (=currentWeekData)
        const findWeekData = datas.dataPregnancyByWeek.find(data => data.week === diffWeek)
        setCurrentWeekData(findWeekData);

        //faire un find pour trouver l'entrée correspondant au trimestre dans data, puis faire un state avec cette data (=currentTrimesterData)
        const findTrimesterDataTmp = datas.symptomsListByTrimester.filter(data => data.trimester === diffTrimester);

        reorganizeTrimesterData(findTrimesterDataTmp);

    }, [globalContext]);


    function reorganizeTrimesterData(findTrimesterDataTmp) {
        if (findTrimesterDataTmp.length === 0) {
            return;
        }


        const findTrimesterData = {
            index : findTrimesterDataTmp[0].index,
            trimester : findTrimesterDataTmp[0].trimester,
            title : findTrimesterDataTmp[0].title,
            datas : [],
        };

        findTrimesterDataTmp.map(({symptom}) => ( 
            findTrimesterData.datas.push({
                symptom,
                
            })
        ));
    
        setCurrentTrimesterData(findTrimesterData);

    }

    if(currentWeekData === null || currentTrimesterData === null) {
        return (<div></div>)
    }
        return (   
            <div>

                <NavigationBar />

                <div className="flex">
                    <div className="mr-6 ml-1">
                        <SymptomsCard 
                            currentTrimesterData={currentTrimesterData}
                                
                        />
                    </div>

                    <div className="p-6 w-4/5">
                        
                            <Typography 
                                variant="h3" 
                                className="mb-4"
                            >
                            Vous êtes à la {currentWeekData.week}ème semaine de grossesse
                            </Typography>

                        <div className="bg-black w-2/4 h-px"></div>
                        
                        <div className="grid grid-rows-3">
                            <figure className="row-span-3">
                                <p className="my-2 text-gray-600">Photo prise sur google image</p>
                                <img src={currentWeekData.image} alt="baby-dev" />
                                <p className="bg-blue-50 my-1 font-serif text-gray-600">Image d'un foetus à la {currentWeekData.week}ème semaine de grossesse</p>
                            </figure>
                            <div className="col-span-3 mr-4">
                                <Typography 
                                    variant="h4" 
                                    className=" italic text-red-300 text-lg mt-10">{currentTrimesterData.title}
                                </Typography>
                            <div className=" text-justify text-xl mr-3">{currentWeekData.content}</div>
                            </div>

                        </div>
                
                    </div>
                    
                </div>

            </div>
        )    
        
}

export default DevBaby;