import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import { GlobalContext } from "../context/GlobalContext";
import Calendar from "react-calendar";
import NavigationBar from "../components/NavBar";

import { Typography,Button } from "@material-tailwind/react";  
import 'react-calendar/dist/Calendar.css';
import { Radio } from "@material-tailwind/react";


function Home() {

    const navigate = useNavigate();

    const { globalContext, setGlobalContext } = useContext(GlobalContext);

    const [recordedDateOnCalendar, setRecordedDateOnCalendar] = useState(null);
    const [formValueChecked, setFormValueChecked] = useState(null);
    const [error, setError] = useState("");

    function handleDateChecked(e) {
        const {value} = e.target
        setFormValueChecked(value);
    }

    function babyDevelopmentOnClick() {

        if (null === recordedDateOnCalendar || null === formValueChecked) {
            setError("Vous devez renseigner les informations")         
        } else if ("ddr" === formValueChecked) {
            let procreationDate = recordedDateOnCalendar;
            procreationDate.setDate(procreationDate.getDate() + 14);
            setGlobalContext({
                ...globalContext,
                dateOfPregnancy: procreationDate,
            }) 
            localStorage.setItem("dateOfPregnancy", JSON.stringify(procreationDate));
            navigate("/dev-baby");
        } else {
            setGlobalContext({
                ...globalContext,
                dateOfPregnancy: recordedDateOnCalendar,
            }) 
            localStorage.setItem("dateOfPregnancy", JSON.stringify(recordedDateOnCalendar));
            navigate("/dev-baby");
        }  
    }


    return (
        <div>
            <NavigationBar />

            <Typography className="text-2xl block m-auto mt-20 mb-10 w-3/5 text-center">Entrez votre date des dernières règles ou votre date de grossesse afin de découvrir l'avancement de votre grossesse</Typography>
                    
                    <div className="flex w-1/4 block m-auto mb-10">

                            <Radio 
                                color="purple"
                                name="dateOfPregnancy" 
                                value="ddr" 
                                label="Date des dernières règle" 
                                onChange={handleDateChecked}
                            />
                            <Radio 
                                color="purple"
                                name="dateOfPregnancy" 
                                value="ddg" 
                                label="Date du début de grossesse" 
                                onChange={handleDateChecked}
                            />        
                        </div>

                        <Calendar 
                            className="block m-auto mb-5" 
                            onChange={setRecordedDateOnCalendar} 
                            value={recordedDateOnCalendar}
                        />


                    <Button 
                        className="shadow bg-red-200 hover:bg-purple-200 focus:shadow-outline focus:outline-none text-white font-bold py-2 px-4 rounded block m-auto" 
                        onClick={babyDevelopmentOnClick}
                    >Quel est le développement de ma grossesse</Button>

                    <div className="error">
                        {error}
                    </div>

        </div>
    )
}

export default Home;