import ReactPlayer from "react-player";

import NavigationBar from "../components/NavBar";

import "../styles/pedagogy.css";
import {
    Typography,
  } from "@material-tailwind/react";

function Pedagogy() {
    return (
        <div>
            <NavigationBar />
            <Typography 
                    variant="h3" 
                    className="text-center mt-9 mb-8"
            >L'accouchement</Typography>
            <div className="all-videos">
                <div >
                    <h3>Bien se préparer à la naissance</h3>
                    <ReactPlayer 
                        url="https://www.youtube.com/watch?v=ahExD4Mm_eE" 
                        width="400px"
                        height="300px"
                        className="video-1"
                    />
                </div>
                <div >
                    <h3>Comment bien pousser</h3>
                    <ReactPlayer 
                        url="https://www.youtube.com/watch?v=prHr2itHp_w"
                        width="400px"
                        height="300px"
                        className="video-2"
                        />
                </div>
                <div >
                    <h3>Le role du papa pendant l'accouchement</h3>
                    <ReactPlayer 
                        url="https://www.youtube.com/watch?v=UM4mIgAwpng"
                        width="400px"
                        height="300px"
                        className="video-3"
                    />
                </div>
            </div>
        </div>
    )
}

export default Pedagogy;