import { Fragment, useState } from "react";

import "../styles/index.css";
import logo from "../assets/pregnant-yoga.jpg"

import {
    Card,
    CardHeader,
    CardBody,
    Typography,
    Button,
    Dialog,
    DialogHeader,
    DialogBody,
    DialogFooter,
  } from "@material-tailwind/react";
  import { Disclosure } from '@headlessui/react'


function SymptomsCard({currentTrimesterData}) {

    const [symptomSelected, setSymptomSelected] = useState(null);
    const [open, setOpen] = useState(false);
 
    //Fonction pour ouvrir la Disclosure du symptome séléctionné et ouvrir le Dialog du symptome 
    function handleOpen(symptom) {
        setSymptomSelected(symptom);
        setOpen(true);

    }

    //Fonction pour refermer le Dialog du symptome
    function handleClose() {
        setOpen(false)
    }
       

    return (
        <div>     
            <Card className="static block m-auto w-96">

                <Typography 
                    variant="h5"
                    className="text-center py-1"
                >La carte des symptômes
                </Typography>
                <div className="bg-black w-2/4 h-px block m-auto"></div>
                <Typography
                    variant="h6"
                    className="text-center text-red-300 mb-2 mt-2"
                >Trimestre {currentTrimesterData.trimester}</Typography>

                <CardHeader  
                    className="static h-56 mt-3">
                        
                    <img
                        src={logo}
                        alt="img-preganant-woman"
                        className="h-full w-full"
                    />
                </CardHeader> 
                <CardBody className="text-center">
                    <Typography variant="h5" className="mb-2">
                        {currentTrimesterData.datas.map(({symptom}) => (
                            <>
                                <Disclosure >
                                        <Disclosure.Button 
                                            className="py-2 flex flex-col block m-auto mb-2 text-red-200 text-xl font-serif mt-8"  
                                            key={symptom.tips}
                                            color="pink"
                                            onClick={() => setSymptomSelected(symptom)}
                                        >
                                        {symptom.title}
                                        </Disclosure.Button>
                                    
                                        <Disclosure.Panel 
                                            className="flex flex-col text-sm text-gray-500"
                                            key={symptom.details}
                                        >
                                        <div className="mb-3">
                                            {symptom.details}
                                        </div>
                                        {symptomSelected &&
                                            <Fragment>
                                                <Button 
                                                    onClick={() => handleOpen(symptom)} 
                                                    variant="text" 
                                                    className="bg-red-100 text-black w-2/4 block m-auto font-serif mb-10 mt-4" 
                                                    key={symptomSelected.details}
                                                >
                                                    Conseils
                                                </Button>
                                                <Dialog open={open} >
                                                    <DialogHeader>
                                                        <Typography className="text-black font-serif text-xl">
                                                            Conseils
                                                        </Typography>   
                                                    </DialogHeader>
                                                    <Typography className="text-sm font-serif text-base ml-4"> 
                                                            {symptomSelected.title} 
                                                    </Typography>
                                                    <DialogBody divider >
                                                    {symptomSelected.tips}
                                                    </DialogBody>
                                                    <DialogFooter>
                                                        <Button
                                                            variant="text"
                                                            color="red"
                                                            onClick={handleClose}
                                                            className="mr-1"
                                                        >
                                                            <span>Fermer</span>
                                                        </Button>
                                                    </DialogFooter>
                                                </Dialog>
                                            </Fragment>
                                        }
                                        </Disclosure.Panel>  
                                </Disclosure>
                            </>
                        ))}
                    </Typography>
                    
                </CardBody>
            </Card>
        </div>
    )
}

export default SymptomsCard;