import semaine1 from "../assets/1-semaine.jpeg";
import semaine2 from "../assets/2-semaine.jpeg";
import semaine3 from "../assets/3-semaine.jpeg";
import semaine4 from "../assets/4-semaine.jpeg";
import semaine5 from "../assets/5-semaine.jpeg";
import semaine6 from "../assets/6-semaine.jpeg";
import semaine7 from "../assets/7-semaine.gif";
import semaine8 from "../assets/8-semaine.jpeg";
import semaine9 from "../assets/9-semaine.jpeg";
import semaine10 from "../assets/10-semaine.jpeg";
import semaine11 from "../assets/11-semaine.jpeg";
import semaine12 from "../assets/12-semaine.jpg";
import semaine13 from "../assets/13-semaine.jpeg";
import semaine14 from "../assets/14-semaine.jpg";
import semaine15 from "../assets/15-semaine.jpeg";
import semaine16 from "../assets/16-semaine.jpeg";
import semaine17 from "../assets/17-semaine.jpeg";
import semaine18 from "../assets/18-semaine.jpeg";
import semaine19 from "../assets/19-semaine.avif";
import semaine20 from "../assets/20-semaine.jpeg";
import semaine21 from "../assets/21-semaine.jpeg";
import semaine22 from "../assets/22-semaine.jpeg";
import semaine23 from "../assets/23-semaine.jpeg";
import semaine24 from "../assets/24-semaine.jpeg";
import semaine25 from "../assets/25-semaine.jpeg";
import semaine26 from "../assets/26-semaine.webp";
import semaine27 from "../assets/27-semaine.jpeg";
import semaine28 from "../assets/28-semaine.jpg";
import semaine29 from "../assets/29-semaine.jpeg";
import semaine30 from "../assets/30-semaine.jpeg";
import semaine31 from "../assets/31-semaine.jpeg";
import semaine32 from "../assets/32-semaine.jpeg";
import semaine33 from "../assets/33-semaine.jpeg";
import semaine34 from "../assets/34-semaine.jpeg";
import semaine35 from "../assets/35-semaine.jpeg";
import semaine36 from "../assets/36-semaine.jpeg";
import semaine37 from "../assets/37-semaine.jpeg";
import semaine38 from "../assets/38-semaine.png";







const dataPregnancyByWeek = [

        {
            week: 1,
            image: semaine1,
            content: "A une semaine de grossesse, vous ne le savez pas encore mais un petit être commence déjà à se former en vous. Vous en êtes déjà à votre 3ème semaine d'aménorrhée, et c'est le début du premier mois de grossesse. Les signes biologiques propres à une grossesse ne se laissent pas entrevoir, et pourtant une série de transformations s'opère déjà chez la future maman. A ce stade, on ne peut pas vraiment encore parler de bébé, mais juste d'un œuf qui commence à se nicher tranquillement. "
        },
        {
            week: 2,
            image: semaine2,
            content: "Après la première semaine qui se passe sans véritable manifestation apparente, la deuxième semaine de grossesse se passe un peu dans les mêmes conditions : toujours aucun symptôme réel. Cependant, votre intuition vous parle et de micro-sensations soulèvent un doute en vous. Vous pressentez que quelque chose se prépare. C'est donc le moment de faire le test de grossesse ! A l'intérieur, on ne vous a pas attendu et le processus suit son cours. D'œuf, on passe en effet à un embryon bien implanté qui va évoluer progressivement au cours des semaines à suivre. Après le test de grossesse et sa confirmation, pour vous, vient la phase de la prudence et du retour à une bonne hygiène de vie."
        },
        {
            week: 3,
            image: semaine3,
            content: "Au début de la troisième semaine de grossesse, votre état doit normalement avoir été confirmé. Si vous êtes toujours dans le doute, c'estle moment d'effectuer les tests (urinaire et prise de sang) nécessaires pour rejoindre officiellement le club des futures mamans. Vous appréhenderez d'ailleurs ainsi mieux les différents changements qui s'opèrent sur votre corps, mais aussi dans votre état psychique. Les hormones sont en effet déjà à l'œuvre et les premiers symptômes se font déjà ressentir, en général. À l'intérieur, on ne vous attend pas ; l'embryon se développe très vite et chaque jour davantage. À partir de maintenant, vous devez donc vraiment commencer à vous mettre dans votre peau de futur parent."
        },
        {
            week: 4,
            image: semaine4,
            content: "Vous voilà à votre 4ème semaine de grossesse, soit la 6ème semaine d'aménorrhée. Vous vous habituez peu à peu à ce petit être que vous sentez à peine et dont la présence commence à bouleverser votre vie. Bébé commence à s'installer même si sa taille et son poids sont encore infimes. Pour la maman, le premier mois rime avec le premier rendez-vous chez le gynéco. C'est maintenant que commence l'aventure prénatale."
        },
        {
            week: 5,
            image: semaine5,
            content: "La 5ème semaine de marque la fin du 1er mois et le début du 2ème mois de grossesse. A cette étape, beaucoup de changements s'opèrent au niveau de bébé : le nez, la bouche et les yeux font notamment leur apparition, même si ce ne sont encore que de petits orifices. Pour maman par contre, les manifestations ne sont pas si différentes de celles de la semaine précédente. Hormis l'action des hormones et quelques légers désagréments, tout va relativement bien. Pour le moment, le plus important est de se ménager et de respecter certaines prescriptions au quotidien pour son bien-être et celui du futur bébé. C'est également déjà le moment de penser à l'accouchement. Et oui, déjà !"
        },
        {
            week: 6,
            image: semaine6,
            content: "La sixième semaine de grossesse, soit la 8ème semaine d'aménorrhée, est caractérisée par un pic de production hormonale dans le sang de la maman. Les petits symptômes désagréables se multiplient et c'est loin d'être plaisant tous les jours. En fait, cette semaine, vous n'êtes pas du tout dans votre assiette. Bébé, pour sa part, poursuit sa croissance rapide, prenant vraiment une forme humanoïde. C'est le moment de faire un entretien prénatal précoce et de trouver de petites astuces pour dissimuler encore un peu votre grossesse."
        },
        {
            week: 7,
            image: semaine7,
            content: "Vous voilà à 7 semaines de grossesse, et vous avez dépassé la moitié du 2ème mois déjà. Les dernières semaines n'ont clairement pas été de tout repos, aussi bien pour vous que pour le petit bébé qui se développe en vous. Et cette semaine ne s'annonce pas vraiment calme non plus. En fait, au cours de cette 9ème semaine d'aménorrhée, les choses vont vraiment s'accélérer pour bébé qui prend complètement forme et va même commencer à bouger. Cependant, il faudra attendre encore quelques semaines pour sentir ses mouvements. De votre côté, cela ne va toujours pas très fort. Les symptômes désagréables de la semaine écoulée se poursuivent sans réelle conséquence."
        },
        {
            week: 8,
            image: semaine8,
            content: "La 8ème semaine de grossesse (10ème semaine d'aménorrhée) marque la fin du 2ème mois de grossesse. L'embryon va devenir fœtus, et c'est précisément cette semaine qu'a lieu l'organogénèse : la création de tous ses organes vitaux. Son développement s'accélère ! Pour la future maman les désagréments sont encore bien présents : nausées, fatigues, maux de ventre… Vous vous sentez bien enceinte ! Il ne manque plus qu'un début de ventre arrondi pour vous le confirmer. Au programme cette semaine : du repos, une alimentation saine et équilibrée accompagnée de beaucoup d'eau."
        },
        {
            week: 9,
            image: semaine9,
            content: "Vous constatez clairement certains changements au niveau de votre corps et vous commencez par avoir des envies bizarres ? C'est normal, vous êtes au début de la 9ème semaine de votre grossesse, ce qui correspond à la 1ère semaine du troisième mois et à la 11ème semaine d'aménorrhée. Cette période est surtout caractérisée par le début des envies de grossesse et par quelques transformations physiques majeures chez la mère. Désormais, il va vraiment être difficile, voire impossible, de cacher votre état. Pendant ce temps, votre bébé poursuit sa croissance. Il a encore beaucoup grandit depuis la semaine dernière et n'est désormais plus un embryon, mais un fœtus. C'est le moment de faire quelques examens obligatoires pour s'assurer que tout va bien, aussi bien de son côté que du vôtre."
        },
        {
            week: 10,
            image: semaine10,
            content: "Ca y est, vous avez atteint la 10ème semaine de grossesse, soit la 12ème semaine d'aménorrhée, ce qui est déjà un grand pas. Vous en êtes au milieu du 3ème mois. Si en plus, vous avez pu respecter toutes nos petites instructions précédentes et que vous vous sentez relativement en bonne forme, alors c'est parfait. Si ce n'est pas le cas, rassurez-vous car cette semaine s'annonce plutôt détendue. Et si tout va bien depuis le début, alors tout va bien ! Les hormones chutent et vous serez donc normalement moins irritable qu'au cours des précédentes semaines. De même, les petits symptômes désagréables s'estompent un peu. Du côté de bébé, tout continue d'aller très vite et il se dessine de plus en plus clairement. Si vous n'avez pas encore effectué le premier examen prénatal, c'est le moment de le faire."
        },
        {
            week: 11,
            image: semaine11,
            content: "Votre ventre s'est encore un peu arrondi et vous avez des envies gourmandes quasi irrésistibles à n'importe quelle heure ? Normal, vous avez entamé votre 11ème semaine de grossesse (13 SA). Et tandis que vos envies se multiplient, votre bébé poursuit sa croissance rapide. Son ossature se forge progressivement et certains de ses organes comme les yeux sont plus distincts. Si vous ne l'avez pas encore fait, c'est peut-être le bon moment pour l'échographie, histoire d'imprimer dès maintenant sa première image dans votre esprit. Quant à vos proches, il est temps de leur annoncer la bonne nouvelle de manière officielle si vous avez jugé bon de garder ça pour vous jusque-là."
        },
        {
            week: 12,
            image: semaine12,
            content: "Cette 12ème semaine est la dernière de votre 3ème mois de grossesse, soit la 14ème semaine d'aménorrhée. Vous êtes quasiment au tiers du parcours. Bravo ! Vous pouvez vous féliciter, surtout que cette semaine et les suivantes s'annoncent un peu éprouvantes. En effet, l'accalmie des deux dernières semaines sera un peu dérangée par le retour de quelques-uns des anciens symptômes et par les grosses fatigues. De même, cette semaine, vous prendrez du poids et vos rondeurs s'accentueront. C'est désormais clair pour tout le monde : vous êtes bien enceinte ! Quant à bébé, il continue de se développer, profitant pleinement de son espace vital. Si vous n'avez pas encore fait le premier examen prénatal, c'est maintenant ou jamais. C'est aussi le moment de penser sérieusement à refaire votre garde-robe."
        },
        {
            week: 13,
            image: semaine13,
            content: "En cette 13ème semaine de grossesse, la dernière du 3ème mois et la 15ème SA, les choses s'annoncent relativement bien pour vous. En effet, comparativement à la semaine écoulée, vous ressentez un certain mieux-être grâce, notamment, à la quasi-disparition des nausées et à la réduction des autres effets désagréables habituels. Vos vêtements ont, certes, du mal à passer, mais vous êtes plus tonique et votre libido est de retour. En ce qui concerne bébé, tout va pour le mieux. Tout se met en place progressivement et il est de plus en plus actif. Pour vous, il est temps de prendre des renseignements sur la maternité et de commencer réellement à vous préparer physiquement et mentalement pour le grand jour."
        },
        {
            week: 14,
            image: semaine14,
            content: "La 14ème semaine de grossesse correspond à la 16ème semaine d'aménorrhée, ce qui veut dire que vous entrez maintenant dans votre 4ème mois de grossesse, soit le début du second trimestre ! Même si ces premières semaines n'ont pas été de tout repos, vous pouvez en effet être fière de vous, car vous venez de passer un cap important dans cette aventure. Vous entamez désormais ce qui est, pour la plupart des femmes enceintes, le meilleur moment de la grossesse. Au cours de cette semaine, différents changements physiques et hormonaux vont ainsi intervenir, tant chez vous que chez votre bébé, vous permettant de constater une évolution palpable des choses. C'est donc le moment de vous rassurer sur le bon déroulement de votre grossesse et d'aborder la suite en toute sérénité, notamment en préparant son arrivée."
        },
        {
            week: 15,
            image: semaine15,
            content: "Déjà 15 semaines que bébé grandit en vous, et enfin, se profile la première vraie période de plaisirs. En effet, pour la plupart des femmes enceintes, la 15ème semaine de grossesse ou 17ème semaine d'aménorrhée est chargée d'émotions partagées avec bébé, et marque le début d'une période plus détendue. Vous êtes au 4ème mois et vous allez enfin connaître des moments de détente. Chez bébé, de nouveaux changements physiques et un regain d'activité vous rassurent de son bon développement. De votre côté, vous retrouvez une certaine joie de vivre ainsi qu'un réel entrain. En fait, vous commencez vraiment à vous épanouir. Cette semaine, vous retournez aussi voir le docteur pour la deuxième consultation prénatale."
        },
        {
            week: 16,
            image: semaine16,
            content: "Pour vous, la 16ème semaine de grossesse, qui correspond à la 18ème semaine d'aménorrhée est un peu dans le prolongement de la semaine précédente. En effet, cette sensation de bien-être et de joie de vivre se prolonge et vous éprouvez vraiment le bonheur d'être enceinte. Et comme pour contribuer encore un peu plus à cette euphorie, bébé commence à bouger dans votre ventre, mais vous ne le sentez pas encore. Certaines le devinent, surtout lorsqu'il s'agit d'une deuxième grossesse. Si ce n'est pas votre cas, rassurez-vous, cela n'a rien d'anormal. Vous pourrez vous aussi bientôt sentir ses mouvements. C'est à partir de la 16ème semaine de grossesse (milieu du 4ème mois), qu'il faut penser à aller faire sa 2ème visite prénatale."
        },
        {
            week: 17,
            image: semaine17,
            content: "Cette semaine, c'est la grande révélation ! En effet, à la 17ème semaine de grossesse, soit la 19ème semaine d'aménorrhée, donc dans la dernière semaine du 4ème mois désormais. Vous pouvez maintenant obtenir LA réponse à la question que vous vous posez sûrement depuis un petit bout de temps. La chambre, plutôt rose ou bleue ? Le futur papa et vous allez enfin être fixés, si vous le souhaitez. Cependant, entre doute et excitation, trouvez tout de même le moyen de vous ménager car certains maux de grossesse feront leur retour : des douleurs et un mal de ventre, notamment. Mais tout cela n'affecte pas votre bébé dont les traits sont clairement définis à présent ! D'ailleurs, n'est-ce pas le bon moment pour commencer à lui choisir un prénom ? Tout se concrétise vraiment à partir de cette semaine."
        },
        {
            week: 18,
            image: semaine18,
            content: "Contrairement aux jours qui viennent de s'écouler, votre 18e semaine de grossesse (20ème semaine d'aménorrhée) s'annonce un peu moins éprouvant pour vous, en dépit de quelques légers désagréments. De façon générale, vous vous sentez mieux. Mais cette semaine marque avant tout le début d'une prise de poids conséquente étalée sur les semaines à venir. Au cours des 15 prochaines semaines, vous allez en effet voir vos formes s'arrondir toujours un peu plus. De son côté, votre bébé poursuit sa croissance et accentue ses mouvements à l'intérieur. Des mouvements que vous pouvez sentir clairement et qui vous rappellent notamment que vous devez effectuer votre 3ème examen prénatal à partir de cette 20ème semaine d'aménorrhée."
        },
        {
            week: 19,
            image: semaine19,
            content: "Votre grossesse en est maintenant à sa 19ème semaine (21e d'aménorrhée), soit à la moitié du 5ème mois ! Que de chemin parcouru et d'épreuves affrontées ! Vous pouvez déjà être fière de vous tout en vous préparant pour les semaines à venir, qui sont généralement relativement moins éprouvantes. Quelques petits désagréments sont toujours présents à cette étape, mais vous vous consolerez assez largement en sentant votre bébé continuer à bouger. En effet, il passe le plus clair de son temps à dormir et il marque encore régulièrement sa présence par des coups de pied et des mouvements plus amples. Plus mobile, plus souple, il vous rappelle également que c'est l'heure d'effectuer le 3ème examen prénatal si vous ne l'avez pas encore fait. Pour profiter des bienfaits du chant prénatal, c'est également le moment."
        },
        {
            week: 20,
            image: semaine20,
            content: "C'est le début d'une nouvelle semaine et visiblement ce n'est pas la pleine forme chez vous. La 20ème semaine de grossesse (soit la 22ème SA, moitié du 5ème mois de grossesse) met visiblement votre organisme à rude épreuve et c'est normal. Bébé continue en effet de se développer, et donc de prendre de la place. Il devient d'ailleurs plus fort au cours de cette semaine, ponctuant ses courts moments d'éveil par de grandes galipettes. Pour tenir le coup, faites un peu comme lui : reposez-vous au maximum pour refaire le plein d'énergie. Par ailleurs, les choses étant à présent claires, organisez une rencontre avec votre responsable afin d'organiser votre congé de maternité."
        },
        {
            week: 21,
            image: semaine21,
            content: "Vous en êtes à votre 21ème semaine de grossesse, cela correspond à la 23ème semaine d'aménorrhée, et vous en êtes au 5ème mois déjà ! Cette semaine se présente plutôt bien pour vous. Les hormones vous sont bénéfiques et vous donnent vraiment l'air de rayonner. D'ailleurs, en dehors de quelques désagréments physiques passagers et des sautes d'humeurs que vous ne vous expliquez pas encore, tout va relativement bien. Chez bébé, c'est un peu pareil. Poursuivant sa croissance et développant encore de nouvelles facultés, il se prépare activement pour le grand jour. Au 5ème mois de grossesse, vous pouvez enfin connaître le sexe de bébé ! C'est le mois idéal pour faire sa 2ème échographie. Vous serez ainsi fixé sur un prénom, sur une couleur… à moins que vous ne préfériez garder le suspense jusqu'au bout !"
        },
        {
            week: 22,
            image: semaine22,
            content: "La 22ème semaine de grossesse correspond à la 24ème semaine d'aménorrhée. Vous êtes encore au 5ème mois, donc dans le 2ème trimestre de grossesse. Tout passe si vite ! Vous devez désormais bien sentir bébé bouger dans votre ventre. Plus mobile que jamais, il se livre à de véritables séances de gymnastique. Cherchant désormais à interagir avec son environnement, il affirme sa présence et en gigotant et en donnant des coups de pieds. Pendant ce temps, vous surveillez votre prise de poids, votre ventre s'arrondit. chaque jour un peu plus. De façon générale, vous n'allez pas trop mal hormis quelques petits désagréments. Avec une grossesse de plus en plus réelle, vous commencez sérieusement à l'accueillir. Pour les plus pressés, il est peut-être temps de créer sa petite liste de naissance et de lui aménager son nouveau nid douillet !"
        },
        {
            week: 23,
            image: semaine23,
            content: "Ça y est ! Vous êtes désormais entrée dans votre sixième mois de grossesse et vous vous rapprochez donc de plus en plus du grand jour. La 23ème semaine correspond à la 25ème semaine d'aménorrhée. À ce stade, votre ventre est déjà bien rond, le cap des nausées matinales est passé et vous sentez de plus en plus la présence de votre bébé. Très actif, votre bébé est dans une phase importante de son développement. S'il vient à naître, votre petit sera un grand prématuré, suivi de près médicalement. En attendant tout va très vite pour lui ! Pas étonnant donc que vous vous sentiez aussi fatiguée. Et puisque l'échéance se rapproche chaque jour un peu plus, il est important de bien s'y préparer en faisant bien attention à votre alimentation et en effectuant, au besoin, certaines démarches administratives avec le futur papa."
        },
        {
            week: 24,
            image: semaine24,
            content: "La 24e semaine de grossesse, correspondant à la 26e semaine d'aménorrhée, s'inscrit un peu dans la continuité de la semaine précédente. Entrant de plain-pied dans votre sixième mois de grossesse, vous vous rapprochez encore un peu plus de l'accouchement. Votre organisme accuse donc un peu le coup, tandis que votre bébé amorce réellement la phase finale de son évolution. Ses organes et ses sens finissent donc de se préparer à la vie de ce côté. Pour l'accueillir comme il se doit, il est donc temps de préparer son trousseau. Pensez tout de même à vous accorder quelques moments de détente pour souffler un coup."
        },
        {
            week: 25,
            image: semaine25,
            content: "Après la phase de croissance intense des deux dernières semaines, bébé ralentit un peu la cadence au cours de la 25e semaine de grossesse, soit la 27ème semaine d'aménorrhée. Vous en êtes au 6ème mois, en fin de deuxième trimestre ! Il continue donc à développer différentes aptitudes, mais il se repose surtout beaucoup plus que par le passé. De votre côté, ce n'est pas vraiment la très grande forme. Il va donc falloir vous ménager. Mais il va aussi et surtout falloir programmer une nouvelle visite chez le gynéco pour le 4ème examen prénatal."
        },
        {
            week: 26,
            image: semaine26,
            content: "Après la phase de croissance intense des deux dernières semaines, bébé ralentit un peu la cadence au cours de la 25e semaine de grossesse, soit la 27ème semaine d'aménorrhée. Vous en êtes au 6ème mois, en fin de deuxième trimestre ! Il continue donc à développer différentes aptitudes, mais il se repose surtout beaucoup plus que par le passé. De votre côté, ce n'est pas vraiment la très grande forme. Il va donc falloir vous ménager. Mais il va aussi et surtout falloir programmer une nouvelle visite chez le gynéco pour le 4ème examen prénatal."
        },
        {
            week: 27,
            image: semaine27,
            content: "Cela fait déjà 27 semaines que vous êtes enceinte. La 27ème semaine de grossesse correspond à la 29ème semaine d'aménorrhée. Cela fait donc 7 mois que vous êtes enceinte ! Vous avez déjà accompli les 2/3 du chemin. Ca n'est pas pour autant que vous ne rencontrez plus ces désagréments désormais habituel pour vous. Votre bébé quant à lui continue d'évoluer à son rythme. Chaque jour, son organisme se renforce et ses sens s'aiguisent. Pour vous, cette semaine reste dans la continuité de la précédente : ménagez-vous et relaxez-vous au maximum, car les derniers mois sont parfois éprouvants…"
        },
        {
            week: 28,
            image: semaine28,
            content: "Voilà 28 semaines que vous êtes enceinte, si l'on compte en semaine de grossesse. En termes d'aménorrhée, c'est la 30ème semaine ! Ce qui fait déjà 7 mois passé de grossesse. Et vous commencez sûrement à trouver le temps un peu long... Il vous reste plus ou moins 10 semaines avant de découvrir votre petit bébé ! En attendant, vous allez encore certainement ressentir de la fatigue ainsi que quelques autres désagréments liés à la grossesse. En ce qui concerne votre bébé, il poursuit son évolution dans votre ventre tout en restant attentif aux moindres stimuli. C'est le moment de choisir la méthode d'alimentation que vous allez adopter pour bébé et de continuer à vous fournir en matériel nécessaire. Pensez aussi aux faire-part afin d'être prêt le jour J !"
        },
        {
            week: 29,
            image: semaine29,
            content: "La 29ème semaine de grossesse, c'est la 31ème semaine d'aménorrhée. Il vous reste moins de deux mois avant le jour J ! Cela correspond au milieu du 7ème mois, une étape primordiale pour le développement du fœtus. Le petit bout de chou qui grandit en vous ressemble d'ailleurs déjà bien au bébé que vous tiendrez bientôt dans vos bras. Tout en poursuivant sa maturation, il réagit en bougeant quand il est face à un stimuli extérieur. En ce qui vous concerne, cette 31ème semaine d'aménorrhée est assez éprouvante. En effet, la fatigue est plus que jamais au rendez-vous, et l'échéance approchant, vous êtes sujette à certains malaises. Ménagez-vous donc autant que possible tout en vous organisant pour la cinquième visite prénatale. C'est déjà également le moment de penser à l'inscription de bébé à la crèche."
        },
        {
            week: 30,
            image: semaine30,
            content: "Vous voici à la fin de votre septième mois de grossesse, ce qui équivaut à la 30ème semaine de grossesse, soit la 32ème semaine d'aménorrhée. Par rapport aux jours qui viennent de s'achever, vous sentirez un nouveau bien-être et moins de fatigue. Pour votre bébé, c'est maintenant que tout se joue : il cherche peut-être déjà à se mettre en position pour l'accouchement, tête vers le bas dans l'utérus. Ce mouvement pourrait vous incommoder quelque peu, mais c'est un moindre mal par rapport à la joie imminente de la naissance. Pour vous relaxer, la sophrologie prénatale pourrait être une excellente alliée."
        },
        {
            week: 31,
            image: semaine31,
            content: "Cette 31ème semaine de grossesse (33ème d'aménorrhée) vous réserve de bien belles choses ! Bébé continue le perfectionnement de son développement. Jour après jour, il se prépare petit à petit à naître. Cet accouchement à venir crée la confusion, vous êtes à la fois pressée et angoissée. Parlez-en autour de vous, c'est un sentiment très commun. Vous rencontrerez également cette semaine de nouveaux kilos sur votre route (au maximum 2 kilo de plus à la 31ème semaine). N'oubliez pas : c'est bénéfique pour bébé ! Prenez le temps de prendre soin de vous, pour mieux connaître et assumer votre corps de future maman. Faîtes-vous chouchouter, et préparez l'arrivée de bébé dans l'environnement le plus zen possible !"
        },
        {
            week: 32,
            image: semaine32,
            content: "À 32 semaines de grossesse (34 semaines d'aménorrhée), vous comptez les jours. Il n'en reste en effet plus beaucoup avant la naissance de bébé. En fait, bébé peut même arriver d'un jour à l'autre. A ce stade, il serait encore prématuré, mais hors de danger. En attendant, votre petit procède aux derniers ajustements, tandis que tout votre corps se prépare pour répondre aux exigences de l'accouchement. Pas étonnant donc que vous soyez très fatiguée, que vous ayez des contractions, ou encore que vos nuits soient marquées de rêves sur la naissance. Prenez votre mal en patience, vous y êtes presque ! Pensez plutôt à programmer votre sixième examen prénatal. Au cours de la semaine, pensez également à faire le point sur votre liste de naissance pour qu'il ne manque vraiment de rien à l'arrivée de bébé."
        },
        {
            week: 33,
            image: semaine33,
            content: "À 33 semaines de grossesse (35 semaines d'aménorrhée), c'est le moment des derniers réglages. Vous en êtes au 8ème mois, et le développement des organes de bébé et leur fonctionnement sont déjà bien évolués. Son organisme peut ainsi déjà se défendre de façon autonome. De votre côté, au fur et à mesure que l'accouchement se rapproche, chaque partie de votre corps vous rappelle que vous donnerez bientôt la vie et qu'il faut donc vous ménager pour être prête le grand jour. Cette semaine, votre sixième examen prénatal est une priorité si vous ne l'aviez pas fait la semaine dernière. Inscrivez-la dans votre agenda, sans oublier de programmer également une rencontre avec l'anesthésiste qui se chargera de vous accompagner le jour de l'accouchement."
        },
        {
            week: 34,
            image: semaine34,
            content: "Voilà maintenant 34 semaines que dure cette belle aventure. Cette 34ème semaine signe la fin du huitième mois de grossesse (déjà), et correspond à la 36ème semaine d'aménorrhée. Plus que quelques jours et ce sera la fin de votre grossesse, mais aussi le début d'une autre aventure tout aussi belle pour vous et pour le bébé qui vit en vous. A ce stade (36 semaines d'aménorrhée), il est d'ailleurs quasiment prêt à venir voir ce qui se passe de ce côté. Son développement étant pratiquement achevé, il se met en position. De votre côté, votre huitième mois de grossesse tirant à sa fin, vous devez commencer votre congé de grossesse afin de vous ménager et de vous préparer. Dans ce cadre, il va falloir dès maintenant, programmer vos premières séances de préparation à l'accouchement pour affronter le grand jour avec sérénité."
        },
        {
            week: 35,
            image: semaine35,
            content: "Félicitations ! Vous entamez votre neuvième et dernier mois de grossesse. Le temps a été long et votre corps a été mis à rude épreuve, pourtant vous avez su tenir le coup. En cette 35ème semaine de grossesse (37 semaines d'aménorrhée), bébé est fin prêt et peut venir au monde sans danger. De votre côté, votre grossesse touchant à son terme, vous êtes de plus en plus impatiente. C'est d'ailleurs peut-être ce qui explique vos changements d'humeur, aussi subits qu'inexpliqués. Tenez bon ! Dans quelques jours, vous pourrez le tenir dans vos bras. En attendant, effectuez votre septième visite prénatale et vérifiez que votre valise de maternité contient bien tout ce qu'il vous faut."
        },
        {
            week: 36,
            image: semaine36,
            content: "Cette semaine vous faites encore un grand pas de plus vers l'heureux évènement que vous attendez avec tant d'impatience. Vous entrez dans votre 36ème semaine de grossesse (38 semaines d'aménorrhée) et bébé est déjà prêt à rencontrer papa et maman. La 36ème semaine de grossesse ou 38ème d'aménorrhée correspond à la deuxième semaine du 9ème mois. Ses sens se sont bien développés et ses organes sont maintenant matures mais il continue à prendre du poids avant sa naissance. De votre côté, vous ressentez plus que jamais le poids de bébé et des kilos que vous avez pris tout au long de votre grossesse. Votre impatience de le voir, mêlée à ces petits désagréments et aux hormones vous font souvent perdre votre calme. Encore un peu de courage ! Bébé se décidera bien assez tôt à vous faire l'honneur de sa venue. Pour l'instant, continuez de préparer son arrivée en complétant son trousseau et essayez de garder toutes vos forces pour le grand jour."
        },
        {
            week: 37,
            image: semaine37,
            content: "Vous y êtes presque ! Cette semaine, vous entrez dans votre 37e semaine de grossesse (39 semaines d'aménorrhée). Plus que quelques jours pour finir votre neuvième mois. Bravo ! Vous avez tenu le coup jusqu'à la toute fin et bébé en a bien profité. Il est maintenant fin prêt, mais décidera de lui-même quand ce sera le bon moment. En dépit de l'impatience et de la fatigue que vous ressentez, il est donc important que vous preniez votre mal en patience et laissiez bébé aller à son rythme. Vous avez su tenir la distance, ne baissez pas les bras maintenant. Tout ce qu'il vous reste à faire, c'est vous reposer, patienter et vous détendre. Ecoutez également les signaux que votre corps vous envoie : douleurs dans le ventre, pertes vaginales, contractions… Tout cela annonce l'arrivée imminente de bébé !"
        },
        {
            week: 38,
            image: semaine38,
            content: "Vous avez atteint la 38e semaine de grossesse (40ème semaine d'aménorrhée) ? Bravo ! Vous êtes à quelques jours de votre date prévue d'accouchement ! Cela veut donc dire que votre bébé a fait le choix d'aller jusqu'au bout du processus de développement à l'intérieur de votre ventre avant de voir le jour. Pour son bien-être et le vôtre, c'est ce qu'il y a de mieux. Malgré votre impatience de l'avoir enfin dans vos bras et en dépit des derniers pics hormonaux, gardez donc votre calme jusqu'au grand jour. Un grand jour qui peut d'ailleurs arriver à tout moment désormais, et pour lequel vous aurez besoin de toute votre énergie. Cette semaine, c'est donc repos et patience au maximum. En attendant, vous pouvez également essayer deux ou trois astuces de grand-mère qui sont censées faciliter l'accouchement."
        },
    ]

    const symptomsListByTrimester = [
        {
            index: 1,
            trimester: 1,
            title: "L'hormone HCG vient troubler la fête, vous vous sentez épuisée, nauséeuse",
            symptom: {
                title: "Fatigue",
                details: "Apparition au bout du 1er mois.",
                tips: "Déléguez les taches du quotidien et dès que vous le pouvez, reposez vous",
            }
            
        },
        {
            index: 2,
            trimester: 1,
            title: "L'hormone HCG vient troubler la fête, vous vous sentez épuisée, nauséeuse",
            symptom: {
                title:"Nausées",
                details: "Au bout du 2ème ou 3ème mois. Même si cela est difficile, rassurez-vous, dans la majorité des cas cet état disparaitra après le 3ème mois.",
                tips: "Voyez avec votre médecin qui pourra vous préscrire un traitement qui diminuera les nausées. Le donormyl et le Cariban ont déjà fait leurs effets",
            },
            
        },
        {
            index: 3,
            trimester: 1,
            title: "L'hormone HCG vient troubler la fête, vous vous sentez épuisée, nauséeuse",
            symptom: { 
                title: "L'absence de règles(aménorrhée)",
                details: "Dès le début de la grossesse",
                tips: "C'est le signe que bébé est bien présent. Attention, des exceptions confirment la règles, il se peut que vous continuiez à avoir vos menstruations. Cela arrive, et ne veut pas dire que bébé n'est pas là. C'est souvent le cas des femmes qui ne se savent pas enceinte.",
            }

        },
        {
            index: 4,
            trimester: 1,
            title: "L'hormone HCG vient troubler la fête, vous vous sentez épuisée, nauséeuse",
            symptom: {
                title: "Brulures d'estomac",
                details: "vives en continu",
                tips: "Restez un maximum dans la position assise ou debout. Quand vous dormez, mettez plus de coussins dans votre dos pour dormir moins allongée. Si cela ne suffit pas, n'hésitez pas à en parler au professionnel qui vous suit.",
            },
            

        },
        {
            index: 5,
            trimester: 1,
            title: "L'hormone HCG vient troubler la fête, vous vous sentez épuisée, nauséeuse",
            symptom: {
                title: "Seins douloureux",
                details: "Les mamelons brunissent",
                tips: "On recommande généralement de porter un soutien-gorge en coton doux, de préférence conçu spécifiquement pour la grossesse et sans armatures, en particulier pour la nuit. Les soutiens-gorge de sport, qui offrent un bon maintien et sont le plus souvent élastiques, peuvent également se révéler de bons investissements.Par ailleurs, pour soulager immédiatement une douleur ou un inconfort, il est généralement efficace d’appliquer un linge mouillé d’eau froide ou au contraire une bouillotte tiède, ou bien encore d’alterner le chaud et le froid.",
            }

        },
        {
            index: 6,
            trimester: 2,
            title: "C'est la fin des nausées et le début de l'épanouissement",
            symptom: {
                title: "Essouflement et vertiges",
                details: "Le second trimestre signe la mise en place définitive du placenta. Cet organe vital pour votre bébé ponctionne une certaine quantité de sang pour lui transmettre tout ce dont il a besoin. Cela peut donc avoir des répercussions chez la future mère : le volume sanguin augmente, et la teneur en fer de celui-ci peut diminuer car tout est redistribué au petit. Cela cause alors une anémie, responsable d'une grande fatigue au quotidien. Chaque effort entraîne un essoufflement, et quelques vertiges. Si au 5ème mois vous vous sentez épuisée, c'est tout à fait normal.",
                tips: "Combattez cette fatigue en faisant une cure de fer, et de vitamines.",
            }

        },
        {
            index: 7,
            trimester: 2,
            title: "C'est la fin des nausées et le début de l'épanouissement",
            symptom: {
                title: "Insomnies",
                details: "Il se peut qu'en fin de trimestre, vous ressentiez des difficultés à vous endormir. Pour beaucoup de femmes, c'est la simple conséquence des angoisses liées à la grossesse. Au second trimestre, votre ventre s'arrondit et votre bébé vous donne quelques coups de pieds. Vous le sentez plus que jamais, et tout devient alors de plus en plus réel. Vers le 6ème mois, vous commencez à vous poser de nombreuses questions : sur l'accouchement, sur le retour à la maison, sur la vie avec bébé… Ces angoisses peuvent alors se traduire sous forme de rêves étranges, parfois des cauchemars. Mais ces angoisses peuvent également être le signe de l'impatience : vous avez hâte que bébé soit là et vous avez du mal à vous endormir ! Ces insomnies sont fréquentes chez les femmes enceintes.",
                tips: "Nous vous recommandons de discuter, tout simplement, afin d'extérioriser vos doutes, ou votre impatience ! Le sommeil est la clé d'une grossesse épanouie.",
            } 

        },
        {
            index: 8,
            trimester: 2,
            title: "C'est la fin des nausées et le début de l'épanouissement",
            symptom: {
                title: "Prise de poids",
                details: "La prise de poids s'enclenche réellement au 2ème trimestre. C'est au bout du 5ème mois généralement qu'on commence à jeter un oeil à la balance. Les quelques kilos que vous avez gagné tournent, en moyenne, autour de 4 à 5kg au milieu du 2ème trimestre. Votre grossesse est unique, certes, vous prendrez donc les kilos que vous devrez prendre ! Mais surveiller la prise de poids est primordial, afin d'éviter les risques d'hypertension qui pourraient être néfaste pour le cœur encore fragile et immature de votre bébé.",
                tips: "Suivez donc bien les conseils du médecin et veillez à vous alimenter de façon saine et équilibrée.",
            }

        },
        {
            index: 9,
            trimester: 3,
            title: "C'est la dernière ligne droite. Courage 💪",
            symptom: {
                title: "Epuisement physique",
                details: "La fatigue est plus que jamais présente dans votre quotidien de future maman. Bébé continue de puiser dans votre énergie pour finir de tout consolider. Il a besoin de graisse, de vitamines, d'oxygène, résultat : une grosse fatigue pour vous. Cela ne s'arrange pas, car il continue sa gymnastique dans votre ventre. A la nuit tombée, bébé s'éveille et s'entraîne à boxer dans votre ventre. Cela vous réveille, vous empêche parfois de dormir. Sans oublier les petites angoisses qui vous travaillent peut-être… En bref : vivement le début du congé maternité pour enfin pouvoir vous reposer ! ",
                tips: "Si vous travaillez, ménagez-vous. De plus, en tant que femme enceinte, votre convention collective vous alloue peut-être un allègement du temps de travail, renseignez-vous auprès de votre employeur pour en bénéficier.",
            }

        },
        {
            index: 10,
            trimester: 3,
            title: "C'est la dernière ligne droite. Courage 💪",
            symptom: {
                title: "Prise de poids",
                details: "Les derniers mois, votre prise de poids va s'accélérer. Bébé a quasiment terminé de constituer tout son système interne, il vous demandera alors énormément de ressources pour stocker la graisse dont il a besoin. En fin de second trimestre, il aura atteint le poids de 1kg, et au cours du dernier trimestre il va donc tripler son poids ! Un bébé atteint en moyenne 3,5kg lorsqu'il nait entre la 36ème semaine de grossesse et la 40ème semaine de grossesse. Cela n'est qu'une approximation, car là encore, chaque grossesse, chaque bébé, et chaque maman et différente ! Pour vous, en revanche, la prise de poids tourne autour de ces 12kg dont tout le monde vous parle. Mais on sait tous que la prise de poids d'une femme enceinte dépend avant tout de sa morphologie. Certaines femmes minces se remplument fortement au cours de ces 9 mois, d'autres ne prendront que 5kg… Tout dépend véritablement de vous, et de l'avis de votre médecin à ce sujet. En fin de grossesse cependant on note une certaine accélération au niveau de la prise de poids, pour toutes les femmes généralement. Et cela est dû à l'appétit de bébé qui vous demande de manger autant que possible !",
                tips: "Ne vous privez pas, et mangez de tout : légumes, fruits, viandes, poissons cuits ! Ces trois derniers mois, faites-vous plaisir pour le bien de bébé ! Réduisez les exercices sportifs à de la marche simple. Si le docteur vous y autorise, faites donc 30 minutes de marche par jour. Cela améliorera votre circulation sanguine et vous détendra.",
            } 

        },
        {
            index: 11,
            trimester: 3,
            title: "C'est la dernière ligne droite. Courage 💪",
            symptom: {
                title: "Angoisses",
                details: "En ce dernier trimestre, on parle d'angoisses, d'inquiétudes, de stress et de peurs diverses. Si vous intériorisez beaucoup vos émotions, vous aurez tendance à connaître quelques insomnies à la nuit tombée. Ou cela risque de se répercuter dans votre sommeil : cauchemars en tout genre peuvent survenir. Vous avez peur de l'accouchement, de la douleur, de la césarienne, de la péridurale… Vous vous posez des questions sur l'aspect de votre bébé, sur le retour à la maison. Mais ces inquiétudes sont tout à fait normales ! ",
                tips: "Nous vous conseillons de poser un maximum de question à tout votre entourage, médecins ou proches, pour vous sentir rassurée. Après tout, vous n'êtes pas la seule à donner la vie, et vous ne serez pas la dernière ! Tout ira très bien.",
            }

        },
        
        
    ]
   

const datas = {
    dataPregnancyByWeek,
    symptomsListByTrimester,
};

export default datas;