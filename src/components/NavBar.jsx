import logo from "../assets/logo-pregnant-woman.jpg"


function NavigationBar() {


    return (
        <div className="mb-40">
            <nav className="fixed top-0 left-0 right-0 w-full bg-red-100 shadow">
                <div className="justify-between px-4 mx-auto lg:max-w-7xl md:items-center md:flex md:px-8">
                    <div className="flex items-center justify-between py-3 md:py-5 md:block">
                        <a href="/">
                            <img className="w-16 rounded-full" src={logo} alt="logo"/>
                        </a>
                    </div>
                    <div>
                        <ul className="items-center justify-center space-y-8 md:flex md:space-x-6 md:space-y-0">
                            <li className="text-white hover:text-indigo-200">
                                <a href="/">Accueil</a>
                            </li>
                        
                            <li className="text-white hover:text-indigo-200">
                                <a href="/pedagogy">L'accouchement</a>
                            </li>
                        </ul>          
                    </div>
                </div>
            </nav>
            </div>
    );
}

export default NavigationBar;