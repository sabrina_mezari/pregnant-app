import { createContext, useState } from "react";

const GlobalContext = createContext();

function GlobalProvider({children}) {

    const [globalContext, setGlobalContext] = useState({
        dateOfPregnancy: localStorage.getItem("dateOfPregnancy"),
    });

    return (
        <GlobalContext.Provider value={{globalContext, setGlobalContext}}>
            {children}
        </GlobalContext.Provider>
    )
    
}

export {
    GlobalContext,
    GlobalProvider
}